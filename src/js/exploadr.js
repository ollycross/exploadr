class Exploadr {
  constructor(selector = 'body', options = {}) {
    this.selector = selector;
    this.element = null;

    this.options = Object.assign({}, {
      innerHTML: '<div><p><i class="fa fa-circle-notch fa-2x fa-spin"></i></p><p>Loading...</p></div>',
      wrapClass: 'exploadr-loader-wrap',
      innerClass: 'exploadr-inner',
      inClass: 'in',
      hasClass: 'has-exploadr',

    }, options);

    this.handleLoaded = this.handleLoaded.bind(this);

    if (/comp|inter|loaded/.test(document.readyState)) {
      this.handleLoaded();
    } else {
      document.addEventListener('DOMContentLoaded', this.handleLoaded);
    }

    return {
      show: this.show.bind(this),
      hide: this.hide.bind(this),
    }
  }

  createLoader() {
    const el = document.createElement('div');
    const inner = document.createElement('div');
    inner.classList.add(this.options.innerClass);
    inner.innerHTML = this.options.innerHTML;
    el.classList.add(this.options.wrapClass);
    el.prepend(inner)
    return el;
  }

  findLoader() {
    if (!this.element.exploadr) {
      this.element.exploadr = this.createLoader();
    }
    return this.element.exploadr;
  }

  fireEvent(name) {
    const event = new CustomEvent(`exploadr${name}`, {
      bubbles: true,
    });
    this.element.dispatchEvent(event);
  }

  has() {
    if (this.element.exploadr) {
      for (let i = this.element.childNodes.length - 1; i >= 0; i--) {
        if (this.element.childNodes[i] === this.element.exploadr) {
          return true;
        }
      }
    }
    return false;
  }

  add() {
    this.element.prepend(this.findLoader());
    this.fireEvent('add');
  }

  hasTransition() {
    const durations = window.getComputedStyle(this.element.exploadr)
      .getPropertyValue('transition-duration')
      .split(',');
    let duration;
    for (var i = durations.length - 1; i >= 0; i--) {
      duration = parseFloat(durations[i].replace(/[^\d\.]/g, ''));
      if (duration !== 0) {
        return true;
      }
    }
    return false;
  }

  show() {
    const fireEvent = (() => {
      this.fireEvent('show');
    }).bind(this);

    const handleTransitionEnd = (() => {
      fireEvent();
      this.element.exploadr.removeEventListener('transitionend', handleTransitionEnd);
    }).bind(this);

    this.fireEvent('willshow');

    if (window.getComputedStyle(this.element).getPropertyValue("position") !== 'relative') {
      this.element.style.position = 'relative';
    }

    if (!this.has()) {
      this.add();
    }

    if (this.hasTransition()) {
      this.element.exploadr.addEventListener('transitionend', handleTransitionEnd);
    } else {
      this.fireEvent('show');
    }

    this.element.exploadr.classList.add(this.options.inClass);
    this.element.classList.add(this.options.hasClass);

    return this;
  }

  hide() {
    const remove = (() => {
      this.remove();
      this.fireEvent('hide');
    }).bind(this);

    const handeTransitionEnd = (() => {
      remove();
      this.element.exploadr.removeEventListener('transitionend', handeTransitionEnd);
    }).bind(this);

    this.fireEvent('willhide');

    if (!this.has()) {
      this.element.prepend(this.findLoader());
    }

    if (this.hasTransition()) {
      this.element.exploadr.addEventListener('transitionend', handeTransitionEnd);
    } else {
      remove();
    }

    this.element.style.position = '';
    this.element.exploadr.classList.remove(this.options.inClass);
    this.element.classList.remove(this.options.hasClass);

    return this;
  }

  remove() {
    if (!this.has()) {
      this.element.prepend(this.findLoader());
    }

    this.fireEvent('remove')

    this.element.removeChild(this.element.exploadr);
  }

  handleLoaded() {
    this.element = typeof this.selector === 'string'
      ? document.querySelector(this.selector)
      : this.selector;
  }
}

export default Exploadr;
