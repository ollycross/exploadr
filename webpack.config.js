const path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => ({
  entry: {
    main: './src/js/exploadr.js',
  },
  devtool: argv.mode === 'development' ? 'source-map' : false,
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: 'exploadr.js',
    libraryTarget: 'umd'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
      },
    }],
  },
  plugins: [
    new WebpackNotifierPlugin({
      alwaysNotify: true,
    }),
    new CopyWebpackPlugin([{
      from: './src/css',
      to: '../css',
    }]),

  ],
});
