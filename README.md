# Exploadr!
A javscript utility to easily add loading overlays to elements.

## Usage

    import Exploadr from 'exploadr'

    // Optionally import default styling
    import 'exploadr/src/css/exploadr.css';

    // Initialise the loader
    const element = document.getElementById('my-element');
    const loader = new Exploadr(element);

    // Show the loader
    loader.start();

    // Do something, then stop the loader
    setTimeout(() => loader.stop(), 5000);


## Arguments
__selector__ _string|element_ The element to add an overlay to.
_default_: `body`

__options__ _object_ Options object (see below)

## Options

__innerHTML__ _string_ The HTML to be displayed within the loader
 _default_: `<div><p><i class="fa fa-circle-o-notch fa-2x fa-spin"></i></p><p>Loading...</p></div>`

__wrapClass__ _string_ The class for the loader wrapper
 _default_: `exploadr-loader-wrap`

__innerClass__ _string_ The class for the loader content
 _default_: `exploadr-inner`

__inClass__ _string_ The class for the loader wrapper 'in' state
 _default_: `in`

__hasClass__ _string_ The class to be added to the element when it has an active loader
 _default_: `has-exploadr`


## Methods
__show ()__ _Exploadr_  Adds + shows the loader overlay

__hide ()__ _Exploadr_  Removes + hides the loader overlay

## Events
__exploadradd__ Fires when the loader overlay element is added to the DOM but before it has shown.

__exploadrwillshow__ Fires before the loader overlay is shown.

__exploadrshow__ Fires when the loader overlay is shown and any transitions have ended.

__exploadrwillhide__ Fires before the loader overlay is hidden.

__exploadrhide__ Fires when the loader overlay is hidden and any transitions have ended.

__exploadrremove__ Fires when the loader overlay element is removed from the DOM.

## Notes
- The default `innerHTML` defined in the `options` hash assumes you are using FontAwesome 5 icons.  If you are not you will need to change the option to suit your use case.
- If the element the loader is added to has `position: static` it will be changed to `position: relative` when the loader is shown, and reverted afterwards.
- There are some default stylesheets included in `/dist/css/exploadr.css`.
- This package is designed to be used with webpack and babel.  The `main` entry in package json points to the ES6 module in `/src`.  If this causes problems with your build process you can find the transpiled and bundled code in the `/dist` folder.
